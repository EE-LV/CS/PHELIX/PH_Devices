﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2014_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2014\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2014_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2014\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This class controls a Synchrolock AP from Coherent.

author: Dennis Neidherr, GSI

License Agreement for this software:

Copyright (C)
Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: D.Beck@gsi.de 

This program is free software; you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation; either version 2 of 
the license, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General License for more details (http://www.gnu.org).

Gesellschaft für Schwerionenforschung, GSI
Planckstr. 1, 64291 Darmstadt, Germany
For all questions and ideas contact: M.Richter@gsi.de, H.Brand@gsi.de or D.Beck@gsi.de.
Last update: 17-JUN-2008</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="PH_SynchrolockAP.i attribute.ctl" Type="VI" URL="../PH_SynchrolockAP.i attribute.ctl"/>
		<Item Name="PH_SynchrolockAP.Lock Mode.ctl" Type="VI" URL="../PH_SynchrolockAP.Lock Mode.ctl"/>
		<Item Name="PH_SynchrolockAP.PC Write PSDB.vi" Type="VI" URL="../PH_SynchrolockAP.PC Write PSDB.vi"/>
		<Item Name="PH_SynchrolockAP.ProcEvents.vi" Type="VI" URL="../PH_SynchrolockAP.ProcEvents.vi"/>
		<Item Name="PH_SynchrolockAP.set GUI Button Elements.vi" Type="VI" URL="../PH_SynchrolockAP.set GUI Button Elements.vi"/>
		<Item Name="PH_SynchrolockAP.set GUI Elements for the first time.vi" Type="VI" URL="../PH_SynchrolockAP.set GUI Elements for the first time.vi"/>
		<Item Name="PH_SynchrolockAP.set GUI Elements.vi" Type="VI" URL="../PH_SynchrolockAP.set GUI Elements.vi"/>
		<Item Name="PH_SynchrolockAP.set i Attr and create DIM Services.vi" Type="VI" URL="../PH_SynchrolockAP.set i Attr and create DIM Services.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="PH_SynchrolockAP.get i attribute.vi" Type="VI" URL="../PH_SynchrolockAP.get i attribute.vi"/>
		<Item Name="PH_SynchrolockAP.i attribute.vi" Type="VI" URL="../PH_SynchrolockAP.i attribute.vi"/>
		<Item Name="PH_SynchrolockAP.ProcCases.vi" Type="VI" URL="../PH_SynchrolockAP.ProcCases.vi"/>
		<Item Name="PH_SynchrolockAP.ProcPeriodic.vi" Type="VI" URL="../PH_SynchrolockAP.ProcPeriodic.vi"/>
		<Item Name="PH_SynchrolockAP.set GUI Refs.vi" Type="VI" URL="../PH_SynchrolockAP.set GUI Refs.vi"/>
		<Item Name="PH_SynchrolockAP.set i attribute.vi" Type="VI" URL="../PH_SynchrolockAP.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="PH_SynchrolockAP.constructor.vi" Type="VI" URL="../PH_SynchrolockAP.constructor.vi"/>
		<Item Name="PH_SynchrolockAP.destructor.vi" Type="VI" URL="../PH_SynchrolockAP.destructor.vi"/>
		<Item Name="PH_SynchrolockAP.evt call ExtTrigger.vi" Type="VI" URL="../PH_SynchrolockAP.evt call ExtTrigger.vi"/>
		<Item Name="PH_SynchrolockAP.evt call Fundamental Enable.vi" Type="VI" URL="../PH_SynchrolockAP.evt call Fundamental Enable.vi"/>
		<Item Name="PH_SynchrolockAP.evt call Fundamental Shift.vi" Type="VI" URL="../PH_SynchrolockAP.evt call Fundamental Shift.vi"/>
		<Item Name="PH_SynchrolockAP.evt call GoLeft.vi" Type="VI" URL="../PH_SynchrolockAP.evt call GoLeft.vi"/>
		<Item Name="PH_SynchrolockAP.evt call GoRight.vi" Type="VI" URL="../PH_SynchrolockAP.evt call GoRight.vi"/>
		<Item Name="PH_SynchrolockAP.evt call Harmonic Enable.vi" Type="VI" URL="../PH_SynchrolockAP.evt call Harmonic Enable.vi"/>
		<Item Name="PH_SynchrolockAP.evt call Harmonic Shift.vi" Type="VI" URL="../PH_SynchrolockAP.evt call Harmonic Shift.vi"/>
		<Item Name="PH_SynchrolockAP.evt call LargeSteps.vi" Type="VI" URL="../PH_SynchrolockAP.evt call LargeSteps.vi"/>
		<Item Name="PH_SynchrolockAP.evt call Lock.vi" Type="VI" URL="../PH_SynchrolockAP.evt call Lock.vi"/>
		<Item Name="PH_SynchrolockAP.evt call Loop Gain.vi" Type="VI" URL="../PH_SynchrolockAP.evt call Loop Gain.vi"/>
		<Item Name="PH_SynchrolockAP.evt call StopMotor.vi" Type="VI" URL="../PH_SynchrolockAP.evt call StopMotor.vi"/>
		<Item Name="PH_SynchrolockAP.get data to modify.vi" Type="VI" URL="../PH_SynchrolockAP.get data to modify.vi"/>
		<Item Name="PH_SynchrolockAP.GUI Refs.ctl" Type="VI" URL="../PH_SynchrolockAP.GUI Refs.ctl"/>
		<Item Name="PH_SynchrolockAP.panel.vi" Type="VI" URL="../PH_SynchrolockAP.panel.vi"/>
		<Item Name="PH_SynchrolockAP.set modified data.vi" Type="VI" URL="../PH_SynchrolockAP.set modified data.vi"/>
	</Item>
	<Item Name="PH_SynchrolockAP.contents.vi" Type="VI" URL="../PH_SynchrolockAP.contents.vi"/>
</Library>

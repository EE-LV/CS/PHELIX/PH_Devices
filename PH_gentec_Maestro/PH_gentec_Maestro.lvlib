﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="PH_gentec_Maestro.Action check connection.vi" Type="VI" URL="../PH_gentec_Maestro.Action check connection.vi"/>
		<Item Name="PH_gentec_Maestro.Action get and set current Settings for the first Time.vi" Type="VI" URL="../PH_gentec_Maestro.Action get and set current Settings for the first Time.vi"/>
		<Item Name="PH_gentec_Maestro.Action get Info.vi" Type="VI" URL="../PH_gentec_Maestro.Action get Info.vi"/>
		<Item Name="PH_gentec_Maestro.Action Measure.vi" Type="VI" URL="../PH_gentec_Maestro.Action Measure.vi"/>
		<Item Name="PH_gentec_Maestro.Action open connection.vi" Type="VI" URL="../PH_gentec_Maestro.Action open connection.vi"/>
		<Item Name="PH_gentec_Maestro.Action set Settings.vi" Type="VI" URL="../PH_gentec_Maestro.Action set Settings.vi"/>
		<Item Name="PH_gentec_Maestro.Data Flow.vi" Type="VI" URL="../PH_gentec_Maestro.Data Flow.vi"/>
		<Item Name="PH_gentec_Maestro.get current Measurement.vi" Type="VI" URL="../PH_gentec_Maestro.get current Measurement.vi"/>
		<Item Name="PH_gentec_Maestro.i attribute.ctl" Type="VI" URL="../PH_gentec_Maestro.i attribute.ctl"/>
		<Item Name="PH_gentec_Maestro.i attribute.vi" Type="VI" URL="../PH_gentec_Maestro.i attribute.vi"/>
		<Item Name="PH_gentec_Maestro.PC Current Measurement.vi" Type="VI" URL="../PH_gentec_Maestro.PC Current Measurement.vi"/>
		<Item Name="PH_gentec_Maestro.PC Write PSDB.vi" Type="VI" URL="../PH_gentec_Maestro.PC Write PSDB.vi"/>
		<Item Name="PH_gentec_Maestro.ProcEvents.vi" Type="VI" URL="../PH_gentec_Maestro.ProcEvents.vi"/>
		<Item Name="PH_gentec_Maestro.range 2 limit.vi" Type="VI" URL="../PH_gentec_Maestro.range 2 limit.vi"/>
		<Item Name="PH_gentec_Maestro.set Autorange.vi" Type="VI" URL="../PH_gentec_Maestro.set Autorange.vi"/>
		<Item Name="PH_gentec_Maestro.set current Settings to GUI.vi" Type="VI" URL="../PH_gentec_Maestro.set current Settings to GUI.vi"/>
		<Item Name="PH_gentec_Maestro.set Data Flow.vi" Type="VI" URL="../PH_gentec_Maestro.set Data Flow.vi"/>
		<Item Name="PH_gentec_Maestro.set Enable Anticipation.vi" Type="VI" URL="../PH_gentec_Maestro.set Enable Anticipation.vi"/>
		<Item Name="PH_gentec_Maestro.set Head Attenuator.vi" Type="VI" URL="../PH_gentec_Maestro.set Head Attenuator.vi"/>
		<Item Name="PH_gentec_Maestro.set i Attr.vi" Type="VI" URL="../PH_gentec_Maestro.set i Attr.vi"/>
		<Item Name="PH_gentec_Maestro.set max. Energy to GUI.vi" Type="VI" URL="../PH_gentec_Maestro.set max. Energy to GUI.vi"/>
		<Item Name="PH_gentec_Maestro.set Measurement Mode.vi" Type="VI" URL="../PH_gentec_Maestro.set Measurement Mode.vi"/>
		<Item Name="PH_gentec_Maestro.set Range.vi" Type="VI" URL="../PH_gentec_Maestro.set Range.vi"/>
		<Item Name="PH_gentec_Maestro.set Sets 2 change Flag.vi" Type="VI" URL="../PH_gentec_Maestro.set Sets 2 change Flag.vi"/>
		<Item Name="PH_gentec_Maestro.set Trigger Level.vi" Type="VI" URL="../PH_gentec_Maestro.set Trigger Level.vi"/>
		<Item Name="PH_gentec_Maestro.set Trigger Source.vi" Type="VI" URL="../PH_gentec_Maestro.set Trigger Source.vi"/>
		<Item Name="PH_gentec_Maestro.set wanted Settings to GUI.vi" Type="VI" URL="../PH_gentec_Maestro.set wanted Settings to GUI.vi"/>
		<Item Name="PH_gentec_Maestro.set Wavelength.vi" Type="VI" URL="../PH_gentec_Maestro.set Wavelength.vi"/>
		<Item Name="PH_gentec_Maestro.set Zero.vi" Type="VI" URL="../PH_gentec_Maestro.set Zero.vi"/>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="PH_gentec_Maestro.get i attribute.vi" Type="VI" URL="../PH_gentec_Maestro.get i attribute.vi"/>
		<Item Name="PH_gentec_Maestro.ProcCases.vi" Type="VI" URL="../PH_gentec_Maestro.ProcCases.vi"/>
		<Item Name="PH_gentec_Maestro.ProcPeriodic.vi" Type="VI" URL="../PH_gentec_Maestro.ProcPeriodic.vi"/>
		<Item Name="PH_gentec_Maestro.set i attribute.vi" Type="VI" URL="../PH_gentec_Maestro.set i attribute.vi"/>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="PH_gentec_Maestro.constructor.vi" Type="VI" URL="../PH_gentec_Maestro.constructor.vi"/>
		<Item Name="PH_gentec_Maestro.destructor.vi" Type="VI" URL="../PH_gentec_Maestro.destructor.vi"/>
		<Item Name="PH_gentec_Maestro.Evt Call Continuous Measurement.vi" Type="VI" URL="../PH_gentec_Maestro.Evt Call Continuous Measurement.vi"/>
		<Item Name="PH_gentec_Maestro.Evt Call Current Measurement.vi" Type="VI" URL="../PH_gentec_Maestro.Evt Call Current Measurement.vi"/>
		<Item Name="PH_gentec_Maestro.Evt Call Set Auto Range.vi" Type="VI" URL="../PH_gentec_Maestro.Evt Call Set Auto Range.vi"/>
		<Item Name="PH_gentec_Maestro.Evt Call Set Trigger Source.vi" Type="VI" URL="../PH_gentec_Maestro.Evt Call Set Trigger Source.vi"/>
		<Item Name="PH_gentec_Maestro.Evt Call Set Waveform Averages.vi" Type="VI" URL="../PH_gentec_Maestro.Evt Call Set Waveform Averages.vi"/>
		<Item Name="PH_gentec_Maestro.Evt Call Zero.vi" Type="VI" URL="../PH_gentec_Maestro.Evt Call Zero.vi"/>
		<Item Name="PH_gentec_Maestro.get data to modify.vi" Type="VI" URL="../PH_gentec_Maestro.get data to modify.vi"/>
		<Item Name="PH_gentec_Maestro.Head Settings.ctl" Type="VI" URL="../PH_gentec_Maestro.Head Settings.ctl"/>
		<Item Name="PH_gentec_Maestro.panel.vi" Type="VI" URL="../PH_gentec_Maestro.panel.vi"/>
		<Item Name="PH_gentec_Maestro.range 2 string.vi" Type="VI" URL="../PH_gentec_Maestro.range 2 string.vi"/>
		<Item Name="PH_gentec_Maestro.set available Range to ring GUI element.vi" Type="VI" URL="../PH_gentec_Maestro.set available Range to ring GUI element.vi"/>
		<Item Name="PH_gentec_Maestro.set modified data.vi" Type="VI" URL="../PH_gentec_Maestro.set modified data.vi"/>
	</Item>
	<Item Name="PH_gentec_Maestro.contents.vi" Type="VI" URL="../PH_gentec_Maestro.contents.vi"/>
	<Item Name="PH_gentec_Maestro_db.ini" Type="Document" URL="../PH_gentec_Maestro_db.ini"/>
</Library>
